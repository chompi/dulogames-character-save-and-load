using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace DuloGames.UI
{
    public class CharacterCreateMgr : MonoBehaviour
    {
        private static CharacterCreateMgr m_Mgr;
        public static CharacterCreateMgr instance
        {
            get { return m_Mgr; }
        }

        [SerializeField] private int m_CharacterSelectSceneId = 0;
        [SerializeField] private InputField m_InputField;
        [SerializeField] private Button m_CreateButton;

        private PlayerClass m_SelectedClass = PlayerClass.Warrior;
        private int m_SelectedGender = 0;
        private int m_SelectedBodyType = 0;
        private int m_SelectedClothType = 0;
        private int m_SelectedSkinColor = 0;

        protected void Awake()
        {
            // Save a reference to the instance
            m_Mgr = this;
        }

        protected void OnEnable()
        {
            if (this.m_CreateButton != null)
                this.m_CreateButton.onClick.AddListener(OnCreatePressed);
        }

        protected void OnDisable()
        {
            if (this.m_CreateButton != null)
                this.m_CreateButton.onClick.RemoveListener(OnCreatePressed);
        }

        protected void OnDestroy()
        {
            m_Mgr = null;
        }

        public void SetSelectedClass(PlayerClass pclass)
        {
            this.m_SelectedClass = pclass;
        }

        public void SetSelectedBodyType(int type)
        {
            this.m_SelectedBodyType = type;
        }

        public void SetSelectedClothType(int type)
        {
            this.m_SelectedClothType = type;
        }

        public void SetSelectedSkinColor(int color)
        {
            this.m_SelectedSkinColor = color;
        }

        public void SetSelectedGender(int gender)
        {
            this.m_SelectedGender = gender;
        }

        public void OnCreatePressed()
        {
            string characterName = this.m_InputField.text;

            // Check if we have entered name
            if (string.IsNullOrEmpty(characterName))
            {
	            Debug.LogWarning("Please enter character name!");
	            return;
            }
			
            // Disable the finish button
            if (this.m_CreateButton != null)
            {
	            this.m_CreateButton.enabled = false;
            }

            // Get the game manager
            GameManager gameManager = GameManager.Instance;

            // Fetch our saves just in case we did not do it early
            // If saves are not fetched before a new save is created
            // We could lose any old saves we had
            gameManager.saveManager.FetchSaves(success =>
            {
                // If fetching was successful and we have saves
                if (success)
                {
                    // Set any character data here, before we save it
                    CharacterInfo info = new CharacterInfo();
                    info.name = characterName;
                    info.raceString = "Human";
                    info.playerClass = (int)this.m_SelectedClass;
                    info.level = 1;

                    // Collect the appearance info
                    AppearanceInfo appearance = new AppearanceInfo();
                    appearance.gender = this.m_SelectedGender;
                    appearance.bodyType = this.m_SelectedBodyType;
                    appearance.clothType = this.m_SelectedClothType;
                    appearance.skinColor = this.m_SelectedSkinColor;

                    // Set the appearance info in the save
                    info.appearance = appearance;

                    // Create the save
                    gameManager.saveManager.CreateSave(info, (saved, saveName) =>
                    {
                        // When the save is create, load the character list scene
                        if (saved)
                        {
                            // Load the scene here
                            SceneManager.LoadScene(this.m_CharacterSelectSceneId);
                        }
                    });
                }
            });
        }
    }
}
