﻿using UnityEngine;
using UnityEngine.UI;

namespace DuloGames.UI
{
    public class ClassToggle : MonoBehaviour
    {
        [SerializeField] private PlayerClass m_Class;

        private Toggle m_Toggle;

        protected void Awake()
        {
            this.m_Toggle = this.gameObject.GetComponent<Toggle>();
        }

        protected void OnEnable()
        {
            if (this.m_Toggle != null)
                this.m_Toggle.onValueChanged.AddListener(OnValueChanged);
        }

        protected void OnDisable()
        {
            if (this.m_Toggle != null)
                this.m_Toggle.onValueChanged.RemoveListener(OnValueChanged);
        }

        public void OnValueChanged(bool selected)
        {
            if (selected)
            {
                if (CharacterCreateMgr.instance != null)
                    CharacterCreateMgr.instance.SetSelectedClass(this.m_Class);
            }
        }
    }
}