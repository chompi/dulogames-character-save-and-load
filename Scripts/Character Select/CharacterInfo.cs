using System;

namespace DuloGames
{
    [Serializable]
    public class CharacterInfo
    {
        public string name;
        public string saveName;
        public string raceString;
        public int playerClass;
        public int level;
        public AppearanceInfo appearance;
    }
}
