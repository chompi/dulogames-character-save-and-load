﻿using UnityEngine;

namespace DuloGames
{
    public class CharacterPreviewMgr : MonoBehaviour
    {
        [SerializeField] private GameObject m_PlayerPrefab;
        [SerializeField] private Transform m_PreviewContainer;

        private GameObject m_CurrentPreviewObject;
        private string m_PreviewingCharacterName = string.Empty;

        public void OnCharacterSelected(CharacterInfo info)
        {
            if (info == null)
                return;

            // Check if already previewing that one
            if (this.m_PreviewingCharacterName.Equals(info.saveName))
                return;

            // Create the preview
            this.CreatePreview(info);

            Debug.Log("Character '" + info.name + "' has been selected.");

            // Set as previewing
            this.m_PreviewingCharacterName = info.saveName;
        }

        public void CreatePreview(CharacterInfo info)
        {
            if (this.m_PlayerPrefab == null)
                return;

            // First delete if there's any old preview
            if (this.m_CurrentPreviewObject != null)
                Destroy(this.m_CurrentPreviewObject);
            
            // Instantiate the character model
			this.m_CurrentPreviewObject = (GameObject)Instantiate(m_PlayerPrefab);
			this.m_CurrentPreviewObject.layer = this.m_PreviewContainer.gameObject.layer;
			this.m_CurrentPreviewObject.transform.SetParent(this.m_PreviewContainer, false);

            // Apply the appearance from the character info
        }
    }
}