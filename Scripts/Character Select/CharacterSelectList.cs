using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DuloGames;

namespace DuloGames.UI
{
    [RequireComponent(typeof(ToggleGroup))]
    public class CharacterSelectList : MonoBehaviour
    {
        [System.Serializable]
        public class OnCharacterSelectedEvent : UnityEvent<CharacterInfo> { }

        [System.Serializable]
        public class OnCharacterDeleteEvent : UnityEvent<CharacterInfo> { }

        [SerializeField] private GameObject m_CharacterPrefab;
        [SerializeField] private Transform m_CharactersContainer;
        
        [Header("Events")]
        [SerializeField] private OnCharacterSelectedEvent m_OnCharacterSelected = new OnCharacterSelectedEvent();
        [SerializeField] private OnCharacterDeleteEvent m_OnCharacterDelete = new OnCharacterDeleteEvent();

        private ToggleGroup m_ToggleGroup;
        private CharacterSelectList_Character m_DeletingCharacter;

        protected void Awake()
        {
            this.m_ToggleGroup = this.gameObject.GetComponent<ToggleGroup>();
        }

        protected void Start()
        {
            // Clear the characters container
            if (this.m_CharactersContainer != null)
            {
                foreach (Transform t in this.m_CharactersContainer)
                    Destroy(t.gameObject);
            }

            // Let's start loading our characters
            GameManager gameManager = GameManager.Instance;

            // Fetch the saves
            gameManager.saveManager.FetchSaves(success =>
            {
                // If fetching was successful and we have saves
                if (success && gameManager.saveManager.HasSaves())
                {
                    bool isFirst = true;

                    // Load each save and create a character entry in the list
                    foreach (string saveName in gameManager.saveManager.saveNames)
                    {
                        gameManager.saveManager.LoadSave(saveName, (loaded, characterInfo) =>
                        {
                            // If the save was loaded
                            if (loaded)
                            {
                                this.AddCharacter(characterInfo, isFirst);
                                isFirst = false;
                            }
                        });
                    }
                }
            });
        }
        
        /// <summary>
        /// Adds a character to the character list.
        /// </summary>
        /// <param name="info">The character info.</param>
        /// <param name="selected">In the character should be selected.</param>
        public void AddCharacter(CharacterInfo info, bool selected)
        {
            if (this.m_CharacterPrefab == null || this.m_CharactersContainer == null)
                return;
            
            // Add the character
            GameObject model = Instantiate<GameObject>(this.m_CharacterPrefab);
            model.layer = this.m_CharactersContainer.gameObject.layer;
            model.transform.SetParent(this.m_CharactersContainer, false);
            model.transform.localScale = this.m_CharacterPrefab.transform.localScale;
            model.transform.localPosition = this.m_CharacterPrefab.transform.localPosition;
            model.transform.localRotation = this.m_CharacterPrefab.transform.localRotation;
            
            // Get the character component
            CharacterSelectList_Character character = model.GetComponent<CharacterSelectList_Character>();

            if (character != null)
            {
                // Set the info
                character.SetCharacterInfo(info);

                // Set the toggle group
                character.SetToggleGroup(this.m_ToggleGroup);

                // Set the selected state
                character.SetSelected(selected);

                // Invoke the select event
                if (selected)
                    this.m_OnCharacterSelected.Invoke(info);

                // Add on select listener
                character.AddOnSelectListener(OnCharacterSelected);

                // Add on delete listener
                character.AddOnDeleteListener(OnCharacterDeleteRequested);
            }
        }

        /// <summary>
        /// Event invoked when when a character in the list is selected.
        /// </summary>
        /// <param name="character">The character.</param>
        private void OnCharacterSelected(CharacterSelectList_Character character)
        {
            if (this.m_OnCharacterSelected != null)
                this.m_OnCharacterSelected.Invoke(character.characterInfo);
        }

        /// <summary>
        /// Event invoked when when a character delete button is pressed.
        /// </summary>
        /// <param name="character">The character.</param>
        private void OnCharacterDeleteRequested(CharacterSelectList_Character character)
        {
            // Save the deleting character reference
            this.m_DeletingCharacter = character;

            // Create a modal box
            UIModalBox box = UIModalBoxManager.Instance.Create(this.gameObject);
            if (box != null)
            {
                box.SetText1("Do you really want to delete this character?");
                box.SetText2("You wont be able to reverse this operation and yourcharcater will be permamently removed.");
                box.SetConfirmButtonText("delete");
                //box.SetCancelButtonText(string.Empty);
                box.onConfirm.AddListener(OnCharacterDeleteConfirm);
                box.onCancel.AddListener(OnCharacterDeleteCancel);
                box.Show();
            }
        }

        /// <summary>
        /// Event invoked when a character deletion is confirmed.
        /// </summary>
        private void OnCharacterDeleteConfirm()
        {
            if (this.m_DeletingCharacter == null)
                return;
            
            // Delete the character save
            GameManager gameManager = GameManager.Instance;

            gameManager.saveManager.DeleteSave(this.m_DeletingCharacter.characterInfo.saveName, (deleted) =>
            {
                // If the delete was successful
                if (deleted)
                {
                    // If this character is selected
                    if (this.m_DeletingCharacter.isSelected && this.m_CharactersContainer != null)
                    {
                        // Find and select new character
                        foreach (Transform t in this.m_CharactersContainer)
                        {
                            CharacterSelectList_Character character = t.gameObject.GetComponent<CharacterSelectList_Character>();

                            // If the character is not the one we are deleting
                            if (!character.Equals(this.m_DeletingCharacter))
                            {
                                character.SetSelected(true);
                                break;
                            }
                        }
                    }

                    // Invoke the on delete event
                    if (this.m_OnCharacterDelete != null)
                        this.m_OnCharacterDelete.Invoke(this.m_DeletingCharacter.characterInfo);

                    // Delete the character game object
                    Destroy(this.m_DeletingCharacter.gameObject);
                }
            });
        }

        /// <summary>
        /// Event invoked when a character deletion is canceled.
        /// </summary>
        private void OnCharacterDeleteCancel()
        {
            this.m_DeletingCharacter = null;
        }
    }
}
