﻿using System;

namespace DuloGames
{
	[Serializable]
	public class AppearanceInfo
	{
		public int gender;
		public int bodyType;
		public int clothType;
		public int skinColor;
	}
}