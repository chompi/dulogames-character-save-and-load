﻿using System;

namespace DuloGames
{
	[Serializable]
	public enum PlayerClass : int
	{
		Warrior = 0,
        Guardian = 1,
        Mage = 2,
        Healer = 3
	}

    public class PlayerClassString
    {
        public static string Get(PlayerClass pclass)
        {
            switch (pclass)
            {
                case PlayerClass.Warrior: return "Warrior";
                case PlayerClass.Guardian: return "Guardian";
                case PlayerClass.Mage: return "Mage";
                case PlayerClass.Healer: return "Healer";
            }

            return "Unknown";
        }
    }
}