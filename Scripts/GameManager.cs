﻿using UnityEngine;
using DuloGames.UI;

namespace DuloGames
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager m_GameManager;

        public static GameManager Instance
        {
            get
            {
                // If game manager is not initialized yet
                if (m_GameManager == null)
                {
                    GameObject go = new GameObject("_GameManager");
                    m_GameManager = go.AddComponent<GameManager>();
                }
                return m_GameManager;
            }
        }
        
        private ISaveManager m_SaveManager;
		
		/// <summary>
		/// Gets the save manager.
		/// </summary>
		/// <value>The save manager.</value>
		public ISaveManager saveManager
		{
			get {
				if (this.m_SaveManager == null)
				{
					// Use the local save manager
					this.m_SaveManager = new LocalSaveManager();
				}
				
				return this.m_SaveManager;
			}
		}

        protected void Awake()
        {
            // Make sure the game manager does not get destroyed
            DontDestroyOnLoad(this.gameObject);

            // Save ref to the game manager
            m_GameManager = this;
        }

        protected void OnDestroy()
        {
            if (m_GameManager.Equals(this))
                m_GameManager = null;
        }
        
        /*
        protected void Start()
        {
            // Let's say you load your characters on start
            // For each character you load you need to do the following

            DuloGames.UI.CharacterInfo info = new DuloGames.UI.CharacterInfo();

            info.name = "Your character name";
            info.raceString = "Your character race";
            info.classString = "Your character class";
            info.level = (int)Random.Range(1, 61);

            this.m_CharacterList.AddCharacter(info, false);
        }
        */
    }
}