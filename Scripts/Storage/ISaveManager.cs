﻿using System;
using System.Collections.Generic;

namespace DuloGames
{
    public interface ISaveManager {
	
        List<string> saveNames { get; }
		
        bool HasSaves();
        void FetchSaves(Action<bool> callback);
        void LoadSave(string saveName, Action<bool, CharacterInfo> callback);
        void CreateSave(CharacterInfo saveData, Action<bool, string> callback);
        void DeleteSave(string saveName, Action<bool> callback);
        void CommitUpdate(CharacterInfo save, Action<bool> callback);
    }
}