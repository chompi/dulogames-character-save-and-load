﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DuloGames
{
	public class LocalSaveManager : ISaveManager {
		
		private List<string> m_SaveNames = new List<string>();
		
		/// <summary>
		/// Gets the save names.
		/// </summary>
		/// <value>The save names.</value>
		public List<string> saveNames
		{
			get { return this.m_SaveNames; }
		}
		
		/// <summary>
		/// Determines whether this instance has saves.
		/// </summary>
		/// <returns><c>true</c> if this instance has characters; otherwise, <c>false</c>.</returns>
		public bool HasSaves()
		{
			return (this.m_SaveNames != null && this.m_SaveNames.Count > 0);
		}
		
		/// <summary>
		/// Writes the save names list to a file.
		/// </summary>
		private void WriteSaveNames()
		{
			string filePath = Application.persistentDataPath + "/savesList.dat";
			
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Create(filePath);
			
			try
			{
				bf.Serialize(file, this.m_SaveNames);
			}
			catch (Exception ex)
			{
				Debug.Log("Error writing saves names to file: " + filePath);
				Debug.Log(ex.Message);
			}
			
			file.Close();
		}
		
		/// <summary>
		/// Fetchs the saves names.
		/// </summary>
		/// <param name="callback">Callback.</param>
		public void FetchSaves(Action<bool> callback)
		{
			string filePath = Application.persistentDataPath + "/savesList.dat";
			bool success = true;
			
			if (File.Exists(filePath))
			{
				BinaryFormatter bf = new BinaryFormatter();
				FileStream file = File.Open(filePath, FileMode.Open);
				
				try
				{
					this.m_SaveNames = (List<string>)bf.Deserialize(file);
				}
				catch (Exception ex)
				{
					Debug.Log("Error loading: " + filePath);
					Debug.Log(ex.Message);
					success = false;
				}
				
				file.Close();
			}
			
			if (callback != null)
				callback.Invoke(success);
		}
		
		/// <summary>
		/// Loads a character save by name.
		/// </summary>
		/// <param name="saveName">Save name.</param>
		/// <param name="callback">Callback.</param>
		public void LoadSave(string saveName, Action<bool, CharacterInfo> callback)
		{
			string filePath = Application.persistentDataPath + "/" + saveName + ".dat";
			
			bool success = true;
			CharacterInfo save = new CharacterInfo();
			
			if (File.Exists(filePath))
			{
				BinaryFormatter bf = new BinaryFormatter();
				FileStream file = File.Open(filePath, FileMode.Open);
				
				try
				{
					save = (CharacterInfo)bf.Deserialize(file);
				}
				catch (Exception ex)
				{
					Debug.Log("Error loading: " + filePath);
					Debug.Log(ex.Message);
					success = false;
				}
				
				file.Close();
			}
			
			if (callback != null)
				callback.Invoke(success, save);
		}
		
		/// <summary>
		/// Creates a save.
		/// </summary>
		/// <param name="saveData">Save data.</param>
		/// <param name="callback">Callback.</param>
		public void CreateSave(CharacterInfo saveData, Action<bool, string> callback)
		{
			// Prepare a save file name
			string fileName = this.PrepareSaveName(saveData.name);
			
			// Assign the save name to the save data
			saveData.saveName = fileName;
			
			// Prepare the file path
			string filePath = Application.persistentDataPath + "/" + fileName + ".dat";
			
			bool success = true;
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Create(filePath);
			
			try
			{
				bf.Serialize(file, saveData);
			}
			catch (Exception ex)
			{
				Debug.Log(ex.Message);
				success = false;
			}
			
			file.Close();
			
			if (success)
			{
				this.m_SaveNames.Add(fileName);
				this.WriteSaveNames();
			}
			
			if (callback != null)
				callback.Invoke(success, fileName);
		}
		
		private string PrepareSaveName(string saveName)
		{
			// Convert spaces
			saveName = saveName.ToLower().Replace(" ", "_");
			
			int numerator = 1;
			string fileName = saveName + "_" + numerator.ToString();
			string filePath = Application.persistentDataPath + "/" + fileName + ".dat";
			
			while (File.Exists(filePath))
			{
				numerator++;
				fileName = saveName + "_" + numerator.ToString();
				filePath = Application.persistentDataPath + "/" + fileName + ".dat";
			}
			
			return fileName;
		}
		
		/// <summary>
		/// Deletes the save.
		/// </summary>
		/// <param name="saveName">Save name.</param>
		/// <param name="callback">Callback.</param>
		public void DeleteSave(string saveName, Action<bool> callback)
		{
			string filePath = Application.persistentDataPath + "/" + saveName + ".dat";
			
			bool success = true;
			
			if (File.Exists(filePath))
			{
				try
				{
					File.Delete(filePath);
				}
				catch (Exception ex)
				{
					Debug.Log(ex.Message);
					success = false;
				}
			}
			
			if (success)
			{
				this.m_SaveNames.Remove(saveName);
				this.WriteSaveNames();
			}
			
			if (callback != null)
				callback.Invoke(success);
		}

		/// <summary>
		/// Commits a save update.
		/// </summary>
		/// <param name="saveData">Save data.</param>
		/// <param name="callback">Callback.</param>
		public void CommitUpdate(CharacterInfo saveData, Action<bool> callback)
		{
			bool success = true;
			string filePath = Application.persistentDataPath + "/" + saveData.saveName + ".dat";
			
			if (File.Exists(filePath))
			{
				BinaryFormatter bf = new BinaryFormatter();
				FileStream file = File.Open(filePath, FileMode.Open, FileAccess.ReadWrite);
				
				try
				{
					bf.Serialize(file, saveData);
				}
				catch (Exception ex)
				{
					Debug.Log(ex.Message);
					success = false;
				}
				
				file.Close();
			}
			else
			{
				Debug.LogError("The save file " + saveData.saveName + " does not exist!");
				success = false;
			}
			
			if (callback != null)
				callback.Invoke(success);
		}
	}
}